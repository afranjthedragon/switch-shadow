﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shopScript : MonoBehaviour {
	public float secondsToDismissScale = 5.0f;
	public float nominalScale = 2.0f;
	public float maximumScale = 4.0f;
	public float radius = 120;
	public float pixelSize = 100;
	private GameObject player;
	private GameObject bubble;
	private GameObject darkness;
	private bool hasBeenTriggered = false;
	private float inGameRadius;
	private float scaleReductionPerSecond;
	private bool expansionEnded = false;
	private float alphaChangePerSecond;
	private GameObject shopUI;

	private float screenHalfHeightInUnits;
	private float screenHalfWidthInUnits;
	// Use this for initialization
	void Start () {
		screenHalfHeightInUnits = Camera.main.orthographicSize;;
		shopUI = GameObject.Find ("Canvas").transform.GetChild(0).gameObject;
		screenHalfWidthInUnits = screenHalfHeightInUnits * Screen.width / Screen.height;
		player = GameObject.Find ("Player");
		inGameRadius = nominalScale * radius / pixelSize;
		scaleReductionPerSecond = nominalScale / secondsToDismissScale;
		bubble = transform.GetChild (3).gameObject;
		alphaChangePerSecond = 0.9f / secondsToDismissScale;
	}
	
	// Update is called once per frame
	void Update () {
		if (darkness == null)
			darkness = GameObject.Find ("Dankness");
		if (player == null)
			player = GameObject.Find ("Player");
		if (expansionEnded) {
			Color col = bubble.GetComponent<SpriteRenderer> ().color;
			bubble.GetComponent<SpriteRenderer> ().color = new Color (col.r, col.g, col.b, col.a - alphaChangePerSecond * Time.deltaTime);
			bubble.transform.localScale = new Vector2(bubble.transform.localScale.x + scaleReductionPerSecond * Time.deltaTime,bubble.transform.localScale.y + scaleReductionPerSecond * Time.deltaTime);
			if (bubble.transform.localScale.x >= maximumScale || (Vector2.Distance(player.transform.position.ToVector2(),transform.position) > bubble.transform.localScale.x * inGameRadius/2.0f) ) {
				shopUI.SetActive (false);
				GameObject.Instantiate (Resources.Load ("shopClosed"), new Vector3 (transform.position.x, transform.position.y, -3), Quaternion.identity);
				darkness.GetComponent<darknessMover> ().inShop = false;
				GameObject.Destroy (gameObject);
				//TODO: Add Effects for dissapearance, USE PULLING FOR THE PARTICLE EFFECT,SHITS EXPENSIVE
			}
		}
			
		if (hasBeenTriggered) {
			darkness.GetComponent<darknessMover> ().shopRimPosition = new Vector4 (transform.position.x - bubble.transform.localScale.x * inGameRadius/2.0f, 0, 0, 0);
			return;
		}
		if (Vector2.Distance (new Vector2(transform.position.x,transform.position.y),new Vector2( player.transform.position.x,player.transform.position.y)) < inGameRadius) {
			shopUI.SetActive (true);
			hasBeenTriggered = true;
			darkness.GetComponent<darknessMover>().inShop = true;
			darkness.GetComponent<darknessMover> ().shopRimPosition = new Vector4 (transform.position.x - bubble.transform.localScale.x * inGameRadius/2.0f, 0, 0, 0);
			GetComponent<Animator> ().SetTrigger ("shouldExpand");
		}
		if (transform.position.x < darkness.GetComponent<MeshRenderer> ().sharedMaterial.GetVector ("_Position").x) {
			GameObject.Instantiate (Resources.Load ("shopClosed"), new Vector3 (transform.position.x, transform.position.y, -3), Quaternion.identity);
			darkness.GetComponent<darknessMover> ().inShop = false;
			shopUI.SetActive (false);
			GameObject.Destroy (gameObject);
		}
	}

	public void expansionEnd()
	{
		expansionEnded = true;
		GameObject.Destroy (GetComponent<Animator> ());
	}
}
