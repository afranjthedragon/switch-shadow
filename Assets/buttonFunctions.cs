﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class buttonFunctions : MonoBehaviour {
	private GameObject shop;
	public void Start()
	{
		shop = GameObject.Find ("ShopUI");
		shop.SetActive (false);
		GameObject.Find ("Dankness").GetComponent<MeshRenderer> ().sharedMaterial.SetVector ("_Position", new Vector4 (-10.0f, 0, 0, 0));
		GameObject.Find ("youDied").GetComponent<Image> ().color = new Color (1, 1, 1, 0);
	}
	public void leftClick(Button button)
	{
		GameObject.Find ("Player").GetComponent<PlatformerCharacterController> ().activatedAddons.Add (PlatformerCharacterController.Addon.EnemyJumper);
		shop.SetActive (false);
		button.interactable = false;
	}

	public void middleClick(Button button)
	{
		GameObject.Find ("Player").GetComponent<PlatformerCharacterController> ().activatedAddons.Add (PlatformerCharacterController.Addon.Powerslide);
		shop.SetActive (false);
		button.interactable = false;
	}
	public void rightClick(Button button)
	{
		GameObject.Find ("Player").GetComponent<PlatformerCharacterController> ().activatedAddons.Add (PlatformerCharacterController.Addon.Dash);
		shop.SetActive (false);
		button.interactable = false;
	}
}
