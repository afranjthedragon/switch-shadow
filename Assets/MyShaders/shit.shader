﻿

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

 Shader "FX/Screen Gray"
{
	Properties
	{
	_Position ("Position", Vector) = (0,0,0,0)
	_NoiseTex("Noise text", 2D) = "white" {}
	_Period ("Period", Range(0,50)) = 1
	_Scale ("Scale", Range(0,100)) = 1
	}
	SubShader
	{
		Tags {
			"Queue"="Overlay+10"
			"RenderType"="Overlay" 
			"ForceNoShadowCasting"="True"
			"IgnoreProjector"="True"
			"DisableBatching" = "True"
		}
		LOD 300
		GrabPass{ "_BackgroundTexture" }
		ZWrite Off
		ZTest Always
		Cull Off
		Blend One Zero

			CGPROGRAM
			#pragma surface surf Lambert vertex:vert
			//#pragma fragment frag
			
			#include "UnityCG.cginc"

			float4 _Position;
			sampler2D _BackgroundTexture;
			sampler2D _NoiseTex;
			float _Period;
			float _Scale;

			struct vertData
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL ;
			};

			struct v2f
			{
				float4 vert : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 worldPos : TEXCOORD1;
				float4 uvgrab: TEXCOORD2;
			};

			struct Input {
            float2 grabUV;
            float4 vert;
            float3 worldPos;
            float2 uv;
            float3 normala;
        };


			void vert (inout vertData v,out Input o)
			{
				o.vert = UnityObjectToClipPos(v.vertex);
				//#ifdef UNITY_UV_STARTS_AT_TOP
				//	v.uv.y = 1-v.uv.y;
				//#endif
				o.grabUV = ComputeGrabScreenPos(o.vert);
            	o.worldPos =  o.vert;
            	o.uv = v.uv;
            	o.normala = v.normal;
			}

			void surf (Input IN, inout SurfaceOutput o) {
            float sinT = sin(_Time.w / _Period);
			float2 distortion = float2(tex2D(_NoiseTex, IN.worldPos.xy / _Scale + float2(sinT, 0) ).r - 0.5,tex2D(_NoiseTex, IN.worldPos.xy / _Scale + float2(0, sinT) ).r -0.5);
			if (IN.worldPos.x > _Position.x + distortion.x)
			{
					o.Albedo = tex2D(_BackgroundTexture, UNITY_PROJ_COORD(IN.grabUV)) * 4;
					return;
			}
			fixed4 col = tex2D(_BackgroundTexture, UNITY_PROJ_COORD(IN.grabUV));
			fixed4 grey = dot(col.rgb, float3(0.3, 0.59, 0.11));
			o.Albedo = grey.rgb * 4;
            }
			ENDCG
	}
}