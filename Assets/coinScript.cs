﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coinScript : MonoBehaviour {
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.name == "Player") {
			//TODO: Implement a coin counter;
			GetComponent<Animator>().SetTrigger("pickUpCoin");
		}
	}
	public void destroyCoin()
	{
		GameObject parent = gameObject.transform.parent.gameObject;
		GameObject.Destroy (gameObject);
		GameObject.Destroy (parent);
	}
}
