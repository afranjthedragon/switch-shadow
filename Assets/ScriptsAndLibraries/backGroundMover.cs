﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class backGroundMover : MonoBehaviour {
	public GameObject player;
	public Camera mainCamera;
	public float cloudSpeed;
	private Vector3 oldPosition;
	private GameObject backLayer;
	private GameObject middleLayer;
	private GameObject frontLayer;
	private float oldCameraPositionX;
	bool firstUpdate;
	// Use this for initialization
	void Start () {
		mainCamera = Camera.main;
		player = GameObject.Find ("Player");
		oldPosition = player.transform.position;
		backLayer = GameObject.Find ("Layer5");
		middleLayer = GameObject.Find ("Layer4");
		frontLayer = GameObject.Find ("Layer3");
		oldCameraPositionX = mainCamera.transform.position.x;
	}

	//Should run once every update
	void FollowPlayer(Vector3 playerPosition)
	{
		middleLayer.GetComponent<MeshRenderer> ().sharedMaterial.mainTextureOffset = new Vector2 (Mathf.Repeat(Time.time * cloudSpeed,1), middleLayer.GetComponent<MeshRenderer> ().sharedMaterial.mainTextureOffset.y);
		if (oldCameraPositionX != mainCamera.transform.position.x)
			frontLayer.GetComponent<MeshRenderer> ().sharedMaterial.mainTextureOffset = new Vector2 (frontLayer.GetComponent<MeshRenderer> ().sharedMaterial.mainTextureOffset.x + (playerPosition.x - oldPosition.x)/50.0f, frontLayer.GetComponent < MeshRenderer> ().sharedMaterial.mainTextureOffset.y);
		frontLayer.transform.position = new Vector3 (mainCamera.transform.position.x,mainCamera.transform.position.y - 5.0f, 1);
		backLayer.transform.position = new Vector3 (mainCamera.transform.position.x, mainCamera.transform.position.y , 3);
		middleLayer.transform.position = new Vector3 (mainCamera.transform.position.x, mainCamera.transform.position.y , 2);
		oldPosition = playerPosition;
		oldCameraPositionX = mainCamera.transform.position.x;
	}
	void Update()
	{
		FollowPlayer (player.transform.position);
	}
}
