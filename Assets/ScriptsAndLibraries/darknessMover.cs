﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class darknessMover : MonoBehaviour {

	Material material;
	Transform cameraTransform;
	Vector3 cameraOldPosition;
	public float darknessSpeed = 0.01f;
	public bool inShop = false;
	public Vector4 shopRimPosition = new Vector4 (0, 0, 0,0);
	// Use this for initialization
	void Start () {
		material = GetComponent<MeshRenderer> ().sharedMaterial;
		cameraTransform = Camera.main.transform;
		cameraOldPosition = cameraTransform.position;
	}

	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (cameraTransform.position.x, cameraTransform.transform.position.y);
		cameraOldPosition = cameraTransform.position;
	}

	void FixedUpdate()
	{
		if (!inShop) {
			material.SetVector ("_Position", new Vector4 (material.GetVector ("_Position").x + darknessSpeed, 0, 0));
		} else if (material.GetVector ("_Position").x >= shopRimPosition.x) {
			material.SetVector ("_Position", shopRimPosition);
		} else {
			material.SetVector ("_Position", new Vector4 (material.GetVector ("_Position").x + darknessSpeed, 0, 0));
		}
	}


}
