﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class hitInfo
{
	public Vector3 point;
	public float angle;
}

public class pointComparer : IComparer<hitInfo>  {

	// Calls CaseInsensitiveComparer.Compare with the parameters reversed.
	public int Compare( hitInfo x, hitInfo y )  {
		if (x.angle < y.angle)
			return -1;
		else if (x.angle > y.angle)
			return 1;
		else
			return 0;
	}
}
public class lightSource : MonoBehaviour {
	public GameObject lightmeshholder;
	private GameObject[] lightObstructors;
	public List<hitInfo> polygonPoints;
	private Vector3[] vertices;
	private Vector2[] vertices2d;
	private int[] triangles;
	//private var vertices2 : Vector3[];
	private Mesh mesh;

	// texture grabber
	private Texture2D texture;
	private int screenwidth;
	private int screenheight;
	private int grab;

	// Use this for initialization
	void Start () {
		lightObstructors = GameObject.FindGameObjectsWithTag ("Obstructor");
		polygonPoints = new List<hitInfo> ();
		screenwidth = Screen.width;
		screenheight = Screen.height;
		var texture = new Texture2D (screenwidth, screenheight, TextureFormat.RGB24, false);
		//	vertices2 = new Vector3[4];
		mesh= lightmeshholder.GetComponent<MeshFilter>().mesh;
	}

	float getAngle(Vector3 input)
	{
		float angle = Vector3.SignedAngle (Vector3.up, input, Vector3.forward);
		return angle;
	}
	private bool myApproximation(float a, float b, float tolerance)
	{
		return (Mathf.Abs(a - b) < tolerance);
	}
	// Update is called once per frame
	void Update () {
		polygonPoints.Clear ();
		Vector2 position = new Vector2 (transform.position.x, transform.position.y);
		foreach (var obstructor in lightObstructors) {
			Vector2[] points = obstructor.GetComponent<PolygonCollider2D> ().points;
			Vector3[] pointess = new Vector3[points.Length];
			for (int i = 0; i < points.Length; ++i) {
				pointess [i] = obstructor.transform.TransformPoint (new Vector3 (points [i].x, points [i].y, 0));
				points [i] = new Vector2 (pointess [i].x, pointess [i].y);
			}
			RaycastHit2D hit;
			foreach (Vector2 point in points) {
				//Debug.DrawRay (transform.position, new Vector3 (point.x, point.y));
				hit = Physics2D.Raycast (new Vector2 (transform.position.x, transform.position.y), point - position, 30.0f);
				if (true)
				{
					//Debug.DrawLine(transform.position,new Vector3(hit.point.x,hit.point.y));
					hitInfo info = new hitInfo ();
					Vector2 dirVector = point - position;
					info.point = hit.point;
					info.angle = getAngle (dirVector);
						polygonPoints.Add (info);
					Vector3 dirVectorRepret = new Vector3 (dirVector.x, dirVector.y, 0);
					Vector3 left = Quaternion.Euler (new Vector3 (0, 0, -0.01f)) * dirVectorRepret;
					Vector3 right = Quaternion.Euler (new Vector3 (0, 0, 0.01f)) * dirVectorRepret;
					hit = Physics2D.Raycast (new Vector2 (transform.position.x, transform.position.y), left, Mathf.Infinity);
					hitInfo info2 = new hitInfo();
					info2.point = hit.point;
					info2.angle = getAngle (left);
					polygonPoints.Add (info2);
					hit = Physics2D.Raycast (new Vector2 (transform.position.x, transform.position.y), right, Mathf.Infinity);
					hitInfo info3 = new hitInfo();
					info3.point = hit.point;
					info3.angle = getAngle (right);
					polygonPoints.Add (info3);
				}
			}
		}
		polygonPoints.Sort (new pointComparer ());
		Vector3[] meshPoints = new Vector3[polygonPoints.Count + 1];
		for (int i = 0; i < polygonPoints.Count; ++i) {
			meshPoints [i] = transform.InverseTransformPoint(polygonPoints [i].point);
		}
		for (int i = 1; i < meshPoints.Length; ++i) {
			Debug.DrawLine (meshPoints [i - 1], meshPoints [i]);
		}
		meshPoints [meshPoints.Length - 1] = new Vector3 (0, 0, 0);
		// triangle list
		int[] triangles = new int[meshPoints.Length * 3];
		int ij = -1;
		for (int n=0;n<triangles.Length-3;n+=3)
		{
			ij++;
			triangles[n] = meshPoints.Length-1;
			if (ij>=meshPoints.Length)
			{
				triangles[n+1] = 0;
				//print ("hit:"+i);
			}else{
				triangles[n+1] = ij+1;
			}
			triangles[n+2] = ij;
		}    
		ij++;
		Vector2[] uv = new Vector2[meshPoints.Length];
		triangles[triangles.Length-3] = meshPoints.Length-1;
		triangles[triangles.Length-2] = 0;
		triangles[triangles.Length-1] = ij-1;

		mesh.vertices = meshPoints;
		mesh.triangles = triangles;
		mesh.uv = uv;
	}

}
	
public class Triangulator
{
	private List<Vector2> m_points = new List<Vector2>();

	public Triangulator (Vector2[] points) {
		m_points = new List<Vector2>(points);
	}

	public int[] Triangulate() {
		List<int> indices = new List<int>();

		int n = m_points.Count;
		if (n < 3)
			return indices.ToArray();

		int[] V = new int[n];
		if (Area() > 0) {
			for (int v = 0; v < n; v++)
				V[v] = v;
		}
		else {
			for (int v = 0; v < n; v++)
				V[v] = (n - 1) - v;
		}

		int nv = n;
		int count = 2 * nv;
		for (int m = 0, v = nv - 1; nv > 2; ) {
			if ((count--) <= 0)
				return indices.ToArray();

			int u = v;
			if (nv <= u)
				u = 0;
			v = u + 1;
			if (nv <= v)
				v = 0;
			int w = v + 1;
			if (nv <= w)
				w = 0;

			if (Snip(u, v, w, nv, V)) {
				int a, b, c, s, t;
				a = V[u];
				b = V[v];
				c = V[w];
				indices.Add(a);
				indices.Add(b);
				indices.Add(c);
				m++;
				for (s = v, t = v + 1; t < nv; s++, t++)
					V[s] = V[t];
				nv--;
				count = 2 * nv;
			}
		}

		indices.Reverse();
		return indices.ToArray();
	}

	private float Area () {
		int n = m_points.Count;
		float A = 0.0f;
		for (int p = n - 1, q = 0; q < n; p = q++) {
			Vector2 pval = m_points[p];
			Vector2 qval = m_points[q];
			A += pval.x * qval.y - qval.x * pval.y;
		}
		return (A * 0.5f);
	}

	private bool Snip (int u, int v, int w, int n, int[] V) {
		int p;
		Vector2 A = m_points[V[u]];
		Vector2 B = m_points[V[v]];
		Vector2 C = m_points[V[w]];
		if (Mathf.Epsilon > (((B.x - A.x) * (C.y - A.y)) - ((B.y - A.y) * (C.x - A.x))))
			return false;
		for (p = 0; p < n; p++) {
			if ((p == u) || (p == v) || (p == w))
				continue;
			Vector2 P = m_points[V[p]];
			if (InsideTriangle(A, B, C, P))
				return false;
		}
		return true;
	}

	private bool InsideTriangle (Vector2 A, Vector2 B, Vector2 C, Vector2 P) {
		float ax, ay, bx, by, cx, cy, apx, apy, bpx, bpy, cpx, cpy;
		float cCROSSap, bCROSScp, aCROSSbp;

		ax = C.x - B.x; ay = C.y - B.y;
		bx = A.x - C.x; by = A.y - C.y;
		cx = B.x - A.x; cy = B.y - A.y;
		apx = P.x - A.x; apy = P.y - A.y;
		bpx = P.x - B.x; bpy = P.y - B.y;
		cpx = P.x - C.x; cpy = P.y - C.y;

		aCROSSbp = ax * bpy - ay * bpx;
		cCROSSap = cx * apy - cy * apx;
		bCROSScp = bx * cpy - by * cpx;

		return ((aCROSSbp >= 0.0f) && (bCROSScp >= 0.0f) && (cCROSSap >= 0.0f));
	}
}