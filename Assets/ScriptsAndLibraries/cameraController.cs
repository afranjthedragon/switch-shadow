﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraController : MonoBehaviour {
	private GameObject player;
	private Vector3 oldPlayerPosition;
	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Player");
		oldPlayerPosition = player.transform.position;
		transform.position = new Vector3 (transform.position.x, player.transform.position.y, transform.position.z);
	}

	// Update is called once per frame
	void Update () {
		float ycam = Mathf.Clamp (0, player.transform.position.y, Mathf.Infinity);
		float xcam = Mathf.Clamp (player.transform.position.x,Camera.main.orthographicSize * Screen.width/Screen.height, 10000);
		transform.position = new Vector3 (xcam, ycam,transform.position.z);
		oldPlayerPosition = player.transform.position;
	}
}
