﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMovement : MonoBehaviour{
	//This is done rather differently than the character controller just because it's easier than going without Rigidbody2D
	//public float HP = 10.0f;
	public float collisionMargin = 0.01f;
	public float velocityFactor = 0.5f;

	enum Direction
	{
		Left = -1,
		Right = 1
	}

	Direction facingDirection;

	private Vector2 Left = new Vector2 (-1, 0);
	private Vector2 Right = new Vector2 (1, 0);
	private Vector2 Up = new Vector2 (0, 1);
	private Vector2 Down = new Vector2 (0, -1);

	private BoxCollider2D boxCollider;
	private Vector2[] rayCastPositions = new Vector2[2];
	private Vector2[] maxPositions = new Vector2[2];
	private bool grounded = false;
	public Rigidbody2D rigidbody;
	public SpriteRenderer renderer;

	// Use this for initialization
	void Start () {
		renderer = GetComponent<SpriteRenderer> ();
		boxCollider = GetComponent<BoxCollider2D> ();
		rigidbody = GetComponent<Rigidbody2D> ();
		int random = Random.Range (0, 1);
		if (random == 0)
			random = -1;
		facingDirection = (Direction)random;
		if (facingDirection == Direction.Left)
			renderer.flipX = false;
		else
			renderer.flipX = true;

	}

	public void Damage()
	{
		renderer.color = new Color (1.0f, 0, 0);
		rigidbody.velocity = (new Vector2 (Random.Range(-1,1), 3.0f));
		GetComponent<Animator> ().SetBool ("isDead", true);
		GetComponent<BoxCollider2D> ().enabled = false;
		StartCoroutine (resetColor());
	}

	IEnumerator resetColor()
	{
		yield return new WaitForSeconds (0.2f);
		gameObject.layer = 12;
		renderer.color = new Color (1, 1, 1);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (gameObject.layer == 12)
			return;
		var rayCastPositions = getRayCastPositions();
		var maxPositions = getMaxYPositions (rayCastPositions);
		if (maxPositions [0].y < maxPositions [1].y) {
			facingDirection = Direction.Right;
			renderer.flipX = true;
		} else if (maxPositions [0].y > maxPositions [1].y) {
			facingDirection = Direction.Left;
			renderer.flipX = false;
		}
		else
		checkIfGrounded ();
		if (GetComponent<BoxCollider2D> ().enabled == false)
			rigidbody.velocity = new Vector2 (rigidbody.velocity.x, rigidbody.velocity.y);
		else {
			if (grounded)
				rigidbody.velocity = (new Vector2 ((int)facingDirection * velocityFactor, 0));
			else
				rigidbody.velocity = (new Vector2 (0, rigidbody.velocity.y));
		}
	}

	Vector2[] getRayCastPositions()
	{
		Vector2 boxCollider = GetComponent<BoxCollider2D> ().size;
		Vector2 position = new Vector2 (GetComponent<BoxCollider2D>().bounds.center.x,GetComponent<BoxCollider2D>().bounds.center.y);
		rayCastPositions [0] = GetComponent<BoxCollider2D>().transform.TransformPoint(new Vector3 (- boxCollider.x / 2 - collisionMargin*2,- boxCollider.y / 2 - collisionMargin*2)).ToVector2();
		rayCastPositions [1] = GetComponent<BoxCollider2D>().transform.TransformPoint(new Vector3 (boxCollider.x / 2 + collisionMargin*2, - boxCollider.y / 2 - collisionMargin*2)).ToVector2();
		return rayCastPositions;
	}

	void checkIfGrounded()
	{
		if ((Mathf.Abs (rayCastPositions [0].y - maxPositions [0].y) < collisionMargin) || Mathf.Abs (rayCastPositions [0].y - maxPositions [1].y) < collisionMargin)
			grounded = true;
		else
			grounded = false;
	}
	Vector2[] getMaxYPositions(Vector2[] raycastPos)
	{
		List<Vector2> YDRayHits = new List<Vector2> ();

		Vector2 boxPosition = new Vector2 (GetComponent<BoxCollider2D>().bounds.center.x,GetComponent<BoxCollider2D>().bounds.center.y);


		for (int i = 0; i < raycastPos.Length; ++i){
			RaycastHit2D hit = Physics2D.Raycast (raycastPos[i], Down,5, ~(1 << 12));
			if (hit.collider != null) {
				maxPositions[i] = hit.point;
			}
			else {
				maxPositions[i] = new Vector2 ( 0,raycastPos[i].y - 20);
			}
		}

		return maxPositions;
	}

	//Helper functions, not putting them in a seperate static class because I'm lazy :) Might do it later
	public Vector2 findMinX(List<Vector2> points)
	{
		Vector2 min = points [0];
		foreach (var point in points)
			if (point.x < min.x)
				min = point;
		return min;
	}

	public Vector2 findMinY(List<Vector2> points)
	{
		Vector2 min = points [0];
		foreach (var point in points)
			if (point.y < min.y)
				min = point;
		return min;
	}

	public Vector2 findMaxX(List<Vector2> points)
	{
		Vector2 min = points [0];
		foreach (var point in points)
			if (point.x > min.x)
				min = point;
		return min;
	}

	public Vector2 findMaxY(List<Vector2> points)
	{
		Vector2 min = points [0];
		foreach (var point in points)
			if (point.y > min.y)
				min = point;
		return min;
	}
}
