﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelManager : MonoBehaviour {
	private LevelLoader loader;
	public int spriteColumns;
	public int spriteRows;
	public Texture2D spriteSheetTexture;
	public int spriteOffset;
	public float pixelsPerUnit = 100;
	public Vector2 levelOffset = new Vector2(0,4);
	// Use this for initialization
	void Start () {
		loader = new LevelLoader ();
		loader.spriteColumns = spriteColumns;
		loader.spriteRows = spriteRows;
		loader.spriteOffset = spriteOffset;
		loader.spriteTexture = spriteSheetTexture;
		loader.pixelsPerUnit = pixelsPerUnit;
		loader.initLevelLoader ();
		loader.ConstructLevel (0, levelOffset);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
