﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public class OgmoLoader {

	private XmlDocument[] levelXmlDocuments;
	public Dictionary<int,OgmoLevel> ogmoLevels;
	public int levelsNum = 0;

	private void getLevelsXML ()
	{
		Object[] levelFileNames = Resources.LoadAll ("Levels");
		levelXmlDocuments = new XmlDocument[levelFileNames.Length];
		for (int i = 0; i < levelFileNames.Length; ++i)
		{
			levelXmlDocuments [i] = new XmlDocument ();
			levelXmlDocuments [i].LoadXml (((TextAsset) levelFileNames [i]).text);
		}
	}

	public void fillOgmoLevels()
	{
		ogmoLevels = new Dictionary<int,OgmoLevel> ();
		getLevelsXML ();
		int levelCounter = 0;
		foreach (XmlDocument doc in levelXmlDocuments)
		{
			ogmoLevels [levelCounter] = new OgmoLevel (doc);
			++levelCounter;
			++levelsNum;
		}
	}

}
