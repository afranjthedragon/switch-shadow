﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public abstract class IOgmoData {
	public abstract void addData (XmlNode node);
}

class Rect
{
	public Rect(int xx, int yy, int ww, int hh)
	{
		x = xx;
		y = yy;
		w = ww;
		h = hh;
	}

	public int x;
	public int y;
	public int w;
	public int h;
}


class RectOgmoData : IOgmoData
{
	public List<Rect> rects;

	public RectOgmoData(XmlNode node)
	{
		addData (node);
	}
	override public void addData (XmlNode node)
	{
		rects = new List<Rect> ();
		for (int i = 0; i < node.ChildNodes.Count; ++i) {

			XmlNode tempNode = node.ChildNodes [i];
			int x = int.Parse (tempNode.Attributes [0].Value);
			int y = int.Parse (tempNode.Attributes [1].Value);
			int w = int.Parse (tempNode.Attributes [2].Value);
			int h = int.Parse (tempNode.Attributes [3].Value);

			rects.Add (new Rect (x, y, w, h));
		}
	}
}

class EntityOgmoData : IOgmoData
{
	public List<Entity> entities;

	public EntityOgmoData(XmlNode node)
	{
		addData (node);
	}
	override public void addData(XmlNode node)
	{
		entities = new List<Entity> ();
		for (int i = 0; i < node.ChildNodes.Count; ++i) {
			XmlNode tempNode = node.ChildNodes [i];
			int x = int.Parse (tempNode.Attributes [1].Value);
			int y = int.Parse (tempNode.Attributes [2].Value);
			string name = tempNode.Name;

			entities.Add (new Entity (name, x, y));
		}
	}
}

class Entity
{
	public Entity(string tip,int xx,int yy)
	{
		type = tip;
		x = xx;
		y = yy;
	}
	public string type;
	public int x;
	public int y;
}
class Tile
{
	public Tile (int xx, int yy, int iid)
	{
		x = xx;
		y = yy;
		id = iid;
	}
	public int x;
	public int y;
	public int id;
}

class TileOgmoData : IOgmoData
{
	public TileOgmoData(XmlNode node)
	{
		addData(node);
	}
	public List<Tile> tiles;
	override public void addData(XmlNode node)
	{
		tiles = new List<Tile> ();
		for (int i = 0; i < node.ChildNodes.Count; ++i) {
			XmlNode tempNode = node.ChildNodes [i];
			int x = int.Parse (tempNode.Attributes [0].Value);
			int y = int.Parse (tempNode.Attributes [1].Value);
			int id = int.Parse (tempNode.Attributes [2].Value);
			tiles.Add (new Tile (x,y,id));
		}
	}
}