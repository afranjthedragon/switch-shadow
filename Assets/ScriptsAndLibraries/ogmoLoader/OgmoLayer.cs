﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public class OgmoLayer {
	public enum Type
	{
		Collision,
		Tile,
		Entity
	};

	public Type type;
	public IOgmoData layerData;

	public void loadData(XmlNode node)
	{
		if (type == Type.Collision) {
			layerData = new RectOgmoData (node);
		} else if (type == Type.Tile) {
			layerData = new TileOgmoData (node);
		} else if (type == Type.Entity) {
			layerData = new EntityOgmoData (node);
		}
	}
	public OgmoLayer(XmlNode node)
	{
		if (node.Name == "collisionLayer")
			type = Type.Collision;
		else if (node.Name == "tileLayer")
			type = Type.Tile;
		else if (node.Name == "objectLayer")
			type = Type.Entity;

		loadData (node);
	}
}
