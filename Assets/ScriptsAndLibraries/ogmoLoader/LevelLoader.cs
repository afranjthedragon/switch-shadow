﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoader{
	OgmoLoader ogmoLoader;
	public Texture2D spriteTexture;
	public int spriteColumns;
	public int spriteRows;
	public int spriteOffset;
	public float defaultSize = 70;
	public float pixelsPerUnit = 70;

	private float screenHeightInUnits = Camera.main.orthographicSize * 2;
	private float screenWidthInUnits;

	private float worldPixelY;
	private float worldPixelX;

	public void initLevelLoader()
	{
		screenWidthInUnits = screenHeightInUnits * Screen.width / Screen.height;
		worldPixelY = screenHeightInUnits / Screen.height;
		worldPixelX = screenWidthInUnits / Screen.width;
		ogmoLoader = new OgmoLoader ();
		ogmoLoader.fillOgmoLevels ();
	}
	public void ConstructLevel(int index, Vector2 centrePos)
	{
		if (index >= ogmoLoader.levelsNum) {
			Debug.Log ("Level out of bounds!");
			return;
		}
		OgmoLevel level = ogmoLoader.ogmoLevels [index];
		foreach (var layerPair in level.ogmoLayers) {
			var layer = layerPair.Value;
			if (layer.type == OgmoLayer.Type.Collision)
				loadCollisionLayer (layer, level.collisionTileSizeX, level.collisionTileSizeY, centrePos);
			else if (layer.type == OgmoLayer.Type.Tile)
				loadTileLayer (layer, level.tileSizeX, level.tileSizeY, centrePos, level.spriteSheetName);
			else if (layer.type == OgmoLayer.Type.Entity)
				loadObjectLayer (layer,centrePos);
		}
	}

	void loadObjectLayer(OgmoLayer layer, Vector2 centrePos)
	{
		EntityOgmoData data = (EntityOgmoData)layer.layerData;
		foreach (Entity entity in data.entities) {
			float x =  centrePos.x + (float)entity.x / (pixelsPerUnit+1);
			float y = centrePos.y - 0.82f - (float)entity.y / (pixelsPerUnit+1);
			if (entity.type == "coin") {
				GameObject.Instantiate (Resources.Load ("coin"), new Vector3 (x+0.1f, y+0.5f, -4), Quaternion.identity);
			} else if (entity.type == "player") {
				GameObject player = GameObject.Instantiate (Resources.Load ("player"), new Vector3 (x+0.1f, y+1.4f, -3.5f), Quaternion.identity) as GameObject;
				player.name = "Player";
			} else if (entity.type == "slime") {
				GameObject.Instantiate (Resources.Load ("slime"), new Vector3 (x+0.1f, y+1.0f, -3), Quaternion.identity);
			} else if (entity.type == "shop") {
				GameObject.Instantiate (Resources.Load ("shop"), new Vector3 (x+0.1f, y+1.25f, -1), Quaternion.identity);
			}
		}
	}
	void loadCollisionLayer(OgmoLayer layer,int tilewidth, int tileheight, Vector2 centrePos)
	{
		RectOgmoData data = (RectOgmoData) layer.layerData;
		//List<List<Vector2>> unoptimizedCollisionPolygons = new List<List<Vector2>>();
		GameObject ColliderHolder = new GameObject ();
		ColliderHolder.tag = "Obstructor";
		foreach(Rect rect in data.rects)
		{
			//List<Vector2> collisionPolygon = new List<Vector2>();
			Vector2 LL = new Vector2(rect.x * (tilewidth)/(pixelsPerUnit+1), -rect.y * (tileheight)/(pixelsPerUnit+1));
			LL += centrePos;
			//collisionPolygon.Add (LL);
			//collisionPolygon.Add (new Vector2 (LL.x + (float)tilewidth * rect.w/pixelsPerUnit, LL.y));
			//collisionPolygon.Add (new Vector2 (LL.x + (float)(tilewidth * rect.w)/pixelsPerUnit, LL.y + rect.h * tileheight / pixelsPerUnit ));
			//collisionPolygon.Add (new Vector2 (LL.x, LL.y + (float)(tileheight * rect.h)/pixelsPerUnit));
			BoxCollider2D collider = ColliderHolder.AddComponent<BoxCollider2D> ();
			var worldCenter = collider.transform.InverseTransformPoint(new Vector3 (LL.x + (float)tilewidth/(pixelsPerUnit+1) * rect.w/2, LL.y - (float)tileheight/(pixelsPerUnit+1) * rect.h/2,0));
			collider.offset = new Vector2 (worldCenter.x, worldCenter.y);
			collider.size = new Vector2(rect.w * tilewidth/(pixelsPerUnit+1),tileheight/(pixelsPerUnit+1) * rect.h);
		}


		//List<List<Vector2>> optimizedCollisionPolygons = polygonsClipper.UniteCollisionPolygons (unoptimizedCollisionPolygons);

	}

	private Vector2 ScreenToWorld2D(Vector2 input)
	{
		Vector3 whyIsUnityThisRetarded = new Vector3 (input.x, input.y);
		Vector3 ret = Camera.main.ScreenToWorldPoint (whyIsUnityThisRetarded);
		return new Vector2 (ret.x, ret.y);
	}

	void loadTileLayer(OgmoLayer layer, int tilewidth, int tileheight, Vector2 centrepos, string spritesheetname)
	{
		TileOgmoData data = (TileOgmoData) layer.layerData;
		foreach (Tile tile in data.tiles) {
			Vector2 holder = new Vector2 (tilewidth, tileheight);
			int id = tile.id;
			float positionx = (id % spriteColumns) * (tilewidth + spriteOffset);
			float positiony = spriteOffset + (spriteRows - id / spriteColumns) * (tilewidth + spriteOffset);

			UnityEngine.Rect rect = new UnityEngine.Rect (new Vector2 (positionx,positiony), new Vector2 (tilewidth, tileheight));
			Sprite sprite = Sprite.Create (spriteTexture,rect, new Vector2 (0,0),pixelsPerUnit);
			GameObject spriteHolder = new GameObject ();
			spriteHolder.name = "Tile";
			SpriteRenderer renderer = spriteHolder.AddComponent<SpriteRenderer> ();
			renderer.sprite = sprite;
			Vector2 pos = new Vector2 ((float)tile.x * (float)tilewidth/(pixelsPerUnit+1),-(tile.y + 1) * tileheight/(pixelsPerUnit+1));
			spriteHolder.transform.position = (new Vector3(pos.x,pos.y) + new Vector3(centrepos.x, centrepos.y));

		}
	}
		
}
