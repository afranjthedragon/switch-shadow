﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public class OgmoLevel {
	
	public int width;
	public int height;
	public int tileSizeX;
	public int tileSizeY;
	public int numTilesX;
	public int numTilesY;
	public int collisionTileSizeX;
	public int collisionTileSizeY;
	public int numTilesXCollision;
	public int numTilesYCollision;
	public string spriteSheetName;

	public Dictionary<string,OgmoLayer> ogmoLayers;

	public OgmoLevel(XmlDocument xmlDoc)
	{
		ogmoLayers = new Dictionary<string,OgmoLayer> ();
		XmlNode root = xmlDoc.FirstChild;
		XmlAttributeCollection attributes = root.Attributes;
		width = int.Parse(attributes.GetNamedItem ("width").Value);
		height = int.Parse(attributes.GetNamedItem ("height").Value);
		tileSizeX = int.Parse(attributes.GetNamedItem ("tileSizeX").Value);
		tileSizeY = int.Parse(attributes.GetNamedItem ("tileSizeY").Value);
		numTilesX = int.Parse(attributes.GetNamedItem ("numTilesX").Value);
		numTilesY = int.Parse(attributes.GetNamedItem ("numTilesY").Value);
		collisionTileSizeX = int.Parse(attributes.GetNamedItem ("collisionTileSizeX").Value);
		collisionTileSizeY = int.Parse(attributes.GetNamedItem ("collisionTileSizeY").Value);
		numTilesXCollision = int.Parse(attributes.GetNamedItem ("numTilesXCollision").Value);
		numTilesYCollision = int.Parse(attributes.GetNamedItem ("numTilesYCollision").Value);

		spriteSheetName = attributes.GetNamedItem ("spriteSheetName").Value;
		for (int i = 0; i < root.ChildNodes.Count; ++i) {
			Debug.Log(root.ChildNodes[i].Name);
			ogmoLayers [root.ChildNodes[i].Name] = new OgmoLayer (root.ChildNodes [i]);
		}
	}
}
