﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlatformerCharacterController : MonoBehaviour{
	Dictionary<Lock,KeyCode> keyBindings = new Dictionary<Lock,KeyCode>();

	public enum Addon
	{
		Powerslide,
		Lamp,
		EnemyJumper,
		Dash
	}

	public List<Addon> activatedAddons = new List<Addon> ();

	public float boostTime = 1.0f;
	public float boostCooldown = 5.0f;
	private bool boosting = false;
	private float timeSinceLastBoost = 5.0f;
	private bool countCooldown = true;

	public float slideDamageDistance = 0.5f;
	public float defaultMaxSpeed = 5.0f;
	public float maxSpeed = 5.0f;
	public float jumpingVelocity = 5.0f;
	public float xAcceleration = 5.0f;
	public float collisionMargin = 0.01f;
	public float angleReductionCoefficient = 1.0f;
	public Vector2 acceleration = new Vector2 (0, -9.81f);
	public Vector2 speed = new Vector2(0,0);


	private int wDirection = 1;

	private Vector2 Left = new Vector2 (-1, 0);
	private Vector2 Right = new Vector2 (1, 0);
	private Vector2 Up = new Vector2 (0, 1);
	private Vector2 Down = new Vector2 (0, -1);

	private Vector2[] frameRaycastPos;
	private Vector2[] thisFramePos;
	private Vector2[] frameMaxPos;
	private Vector2[] belows = new Vector2[2];
	List<int> noHit = new List<int> ();

	private int lockFlag = 0;
	private bool grounded = false;
	private Animator animator;
	private SpriteRenderer renderer;
	private GameObject lantern;
	private GameObject dirt;
	private GameObject boost;
	private GameObject darkness;
	private bool dead = false;
	private GameObject youDied;

	private HashSet<EnemyHitInfo> LEnemies = new HashSet<EnemyHitInfo>();
	private HashSet<EnemyHitInfo> REnemies = new HashSet<EnemyHitInfo>();
	private HashSet<EnemyHitInfo> UEnemies = new HashSet<EnemyHitInfo>();
	private HashSet<EnemyHitInfo> DEnemies = new HashSet<EnemyHitInfo>();
	int jumpStatus = 0;
	public enum Lock
	{
		Left = 1,
		Right = 2,
		Up = 4,
		Down = 8,
		Boost
	}
	// Use this for initialization
	void Start () {
		//TODO:Testing addons
		//activatedAddons.Add(Addon.Powerslide);
		//TODO: Load these from the settings file
		//Alternatively set em up for touch via events
		keyBindings[Lock.Left] = KeyCode.A;
		keyBindings [Lock.Right] = KeyCode.D;
		keyBindings [Lock.Up] = KeyCode.W;
		keyBindings [Lock.Down] = KeyCode.S;
		keyBindings [Lock.Boost] = KeyCode.Space;
		frameRaycastPos = getRayCastPositions ();
		animator = GetComponent<Animator> ();
		renderer = GetComponent<SpriteRenderer> ();
		lantern = GameObject.Find ("lantern");
		dirt = GameObject.Find ("Dirt");
		boost = GameObject.Find ("Boost");
		dirt.SetActive (false);
		boost.SetActive (false);

		darkness = GameObject.Find ("Dankness");
		youDied = GameObject.Find("youDied");
	}

	void checkForDeath()
	{
		if (transform.position.y < -5.5 || transform.position.x < darkness.GetComponent<MeshRenderer>().sharedMaterial.GetVector("_Position").x) {
			dead = true;

		}
	}
	void setToWalk()
	{
		animator.ResetTrigger (Animator.StringToHash ("slide"));
		animator.ResetTrigger (Animator.StringToHash ("stand"));
		animator.ResetTrigger (Animator.StringToHash ("jump"));
		animator.SetBool (Animator.StringToHash ("walk"),true);
		dirt.SetActive (false);
	}

	void setToIdle()
	{
		animator.ResetTrigger (Animator.StringToHash ("slide"));
		animator.SetTrigger (Animator.StringToHash ("stand"));
		animator.ResetTrigger (Animator.StringToHash ("jump"));
		animator.SetBool (Animator.StringToHash ("walk"),false);
		dirt.SetActive (false);
	}

	void setToSlide()
	{
		animator.SetTrigger (Animator.StringToHash ("slide"));
		animator.ResetTrigger (Animator.StringToHash ("stand"));
		animator.ResetTrigger (Animator.StringToHash ("jump"));
		if (activatedAddons.Contains(Addon.Powerslide)) {
			dirt.SetActive (true);
		}
		animator.SetBool (Animator.StringToHash ("walk"),false);
	}

	void setToJump()
	{
		animator.ResetTrigger (Animator.StringToHash ("slide"));
		animator.ResetTrigger (Animator.StringToHash ("stand"));
		animator.SetTrigger (Animator.StringToHash ("jump"));
		animator.SetBool (Animator.StringToHash ("walk"),false);
		dirt.SetActive (false);
	}
	// Update is called once per frame
	void Update()
	{
		if (dead)
			return;
		bool accelerating = false;
		if (Input.GetKey (keyBindings [Lock.Left]) && grounded) {
			accelerateX (-1);
			accelerating = true;
		}
		if (Input.GetKey (keyBindings [Lock.Right]) && grounded) {
			accelerateX (1);
			accelerating = true;
		}
		if (Input.GetKeyDown (keyBindings [Lock.Up]))
			jump ();
		if (Input.GetKeyDown (keyBindings [Lock.Boost]) && timeSinceLastBoost > boostCooldown && activatedAddons.Contains(Addon.Dash))
			boosting = true;
		if (accelerating && grounded)
			setToWalk ();
		else if (!accelerating && grounded && ((speed.x < -maxSpeed / 2.0f) || (speed.x > maxSpeed / 2.0f))) {
			reduceAcceleration (Time.deltaTime);
			setToSlide ();
			if (activatedAddons.Contains(Addon.Powerslide)) {
				dirt.SetActive (true);
			}
		} else if (!accelerating && grounded && speed.x == 0) {
			setToIdle ();
		} else if (!accelerating && grounded)
			reduceAcceleration (Time.deltaTime);
		checkAndSetDirection ();
		checkForDeath ();
	}

	void checkAndSetDirection()
	{
		if (speed.x > 0 && animator.GetBool (Animator.StringToHash ("slide"))) {
			wDirection = 1;
			renderer.flipX = true;
			dirt.transform.rotation = Quaternion.LookRotation (new Vector3 (1, 0, 0));
			boost.transform.rotation = Quaternion.LookRotation (new Vector3 (-1, 0, 0));
		}
		else if (speed.x < 0 && animator.GetBool (Animator.StringToHash ("slide"))) {
			wDirection = -1;
			dirt.transform.rotation = Quaternion.LookRotation (new Vector3 (-1, 0, 0));
			boost.transform.rotation = Quaternion.LookRotation (new Vector3 (1, 0, 0));
			renderer.flipX = false;
		}
		else if (wDirection == 1)
			renderer.flipX = false;
		else if (wDirection == -1)
			renderer.flipX = true;
	}

	void reduceAcceleration(float dt)
	{
		acceleration = new Vector2 (-acceleration.x, acceleration.y);
		if (speed.x > 0) {
			speed = new Vector2 (Mathf.Clamp(speed.x - dt * Mathf.Abs(acceleration.x),0,Mathf.Infinity), speed.y);
		} else if (speed.x < 0) {
			speed = new Vector2 (Mathf.Clamp(speed.x + dt * Mathf.Abs(acceleration.x),-Mathf.Infinity,0), speed.y);
		}
		if (acceleration.x < 0)
			acceleration = new Vector2 (Mathf.Clamp(acceleration.x + Time.deltaTime,-Mathf.Infinity,0),acceleration.y);
		else
			acceleration = new Vector2 (Mathf.Clamp(acceleration.x - Time.deltaTime,0,Mathf.Infinity),acceleration.y);
		if (Mathf.Approximately (speed.x, 0)) {
			acceleration = new Vector2 (0, acceleration.y);
			speed = new Vector2 (0, speed.y);
		}
	}

	void checkForSlide()
	{
		if (!dirt.activeSelf)
			return;
		if (wDirection == 1) {
			foreach (EnemyHitInfo hitInfo in REnemies) {
				if (Mathf.Abs (frameRaycastPos [1].x - hitInfo.position.x) < slideDamageDistance && activatedAddons.Contains(Addon.Powerslide))
					hitInfo.enemy.BroadcastMessage("Damage");
			}
		} else {
			foreach (EnemyHitInfo hitInfo in LEnemies) {
				if (Mathf.Abs (frameRaycastPos [0].x - hitInfo.position.x) < slideDamageDistance && activatedAddons.Contains(Addon.Powerslide))
					hitInfo.enemy.BroadcastMessage("Damage");
			}
		}
			
	}
	void FixedUpdate () {
		if (dead) {
			GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1,Mathf.Clamp(GetComponent<SpriteRenderer> ().color.a - 3 * Time.deltaTime,0,1));
			youDied.GetComponent<Image> ().color = new Color (1, 1, 1, Mathf.Clamp01(youDied.GetComponent<Image>().color.a + Time.deltaTime));
			return;
		}
		checkForEnemyContact ();
		frameMaxPos = getMaxPositions (frameRaycastPos);
		if ((speed.x < 0 && (lockFlag & (int)Lock.Left) == (int)Lock.Left) || (speed.x > 0 && (lockFlag & (int)Lock.Right) == (int)Lock.Right))
			speed = new Vector2 (0, speed.y);
		if ((speed.y < 0 && (lockFlag & (int)Lock.Down) == (int)Lock.Down))
			speed = new Vector2(speed.x,0);
		if ((speed.y < 0 && (lockFlag & (int)Lock.Up) == (int)Lock.Up))
			speed = new Vector2(speed.x,0);
		if (grounded) {
			//rotateToFit ();
			speed = new Vector2 (Mathf.Clamp (speed.x + acceleration.x * Time.deltaTime, -maxSpeed, maxSpeed), 0);
		} else {
			//rotateTowardsMedian ();
			speed = new Vector2 (Mathf.Clamp (speed.x,-maxSpeed, maxSpeed), speed.y + acceleration.y * Time.deltaTime);
		}
		if (boosting) {
			if (Mathf.Abs (speed.x * 2) > maxSpeed) {
				maxSpeed = Mathf.Abs (speed.x * 2);
			}
			speed = new Vector2 (speed.x * 2, speed.y);
			boost.SetActive (true);
			boost.GetComponent<ParticleSystem> ().time = 0;
			boost.GetComponent<ParticleSystem> ().Play ();
			countCooldown = false;
			timeSinceLastBoost = 0;
			StartCoroutine ("resetBoost");
			boosting = false;
		}
		if (animator.GetBool (Animator.StringToHash ("walk")))
			animator.speed = 0.4f + Mathf.Abs(speed.x / maxSpeed);
		else
			animator.speed = 1.0f;
		transform.position = new Vector3 (transform.position.x + speed.x * Time.deltaTime, transform.position.y + speed.y * Time.deltaTime,transform.position.z);
		frameRaycastPos = getRayCastPositions ();
		lockFlag = getLockFlag ();
		checkForGrounded ();
		checkForSlide ();

		if (countCooldown)
			timeSinceLastBoost += Time.deltaTime;
	}

	IEnumerator resetBoost()
	{
		yield return new WaitForSeconds (boostTime);
		maxSpeed = defaultMaxSpeed;
		boost.GetComponent<ParticleSystem> ().Stop ();
		boost.SetActive(false);
		countCooldown = true;
	}

	void jump()
	{
		if (jumpStatus == 0 && !grounded)
			return;
		if (jumpStatus == 2)
			return;
		setToJump ();
		grounded = false;
		++jumpStatus;
		transform.position = new Vector3 (transform.position.x, transform.position.y + collisionMargin + 0.001f,transform.position.z);
		speed = new Vector2 (speed.x,jumpingVelocity);
	}

	void checkForGrounded()
	{
		if (Mathf.Abs (frameRaycastPos [0].y - belows [0].y) > collisionMargin * 4 && Mathf.Abs (frameRaycastPos [1].y - belows [1].y) > collisionMargin * 4) {
			grounded = false;
			setToJump ();
		}
	}
		
	void accelerateX(int direction)
	{
		acceleration = new Vector2 (direction * xAcceleration, acceleration.y);
		wDirection = direction;
	}

	int getLockFlag()
	{

		thisFramePos = frameRaycastPos;
		int flag = 0;
		if (frameRaycastPos [0].x <= frameMaxPos [0].x + collisionMargin || frameRaycastPos [2].x <= frameMaxPos [0].x + collisionMargin) {
			flag |= (int)Lock.Left;
			float diff = frameMaxPos [0].x -  Mathf.Max (thisFramePos [0].x, thisFramePos [2].x)+ collisionMargin;
			transform.position = new Vector3 (transform.position.x + diff, transform.position.y,transform.position.z);
			for (int i = 0; i < frameRaycastPos.Length; ++i)
				frameRaycastPos [i] = new Vector2 (frameRaycastPos [i].x + diff, frameRaycastPos [i].y);
		}
		if ((frameRaycastPos [1].x >= frameMaxPos [1].x - collisionMargin) || frameRaycastPos [3].x >= frameMaxPos [1].x - collisionMargin) {
			flag |= (int)Lock.Right;
			float diff = frameMaxPos [1].x - Mathf.Min (thisFramePos [1].x, thisFramePos [3].x) - collisionMargin;
			transform.position = new Vector3 (transform.position.x + diff, transform.position.y,transform.position.z);
			for (int i = 0; i < frameRaycastPos.Length; ++i)
				frameRaycastPos [i] = new Vector2 (frameRaycastPos [i].x + diff, frameRaycastPos [i].y);
		}
		if ((frameRaycastPos [0].y <= frameMaxPos [2].y + collisionMargin)  || (frameRaycastPos [1].y <= frameMaxPos [2].y + collisionMargin)) {
			//Forget this rotational bullshit
			//float middlePoint = (belows  [0].y + belows [1].y) / 2.0f;
			Vector2 referencePoint = thisFramePos[0];
			float diff = Mathf.Max(belows[0].y,belows[1].y) - referencePoint.y + collisionMargin * 2;
			transform.position = new Vector3 (transform.position.x, transform.position.y + diff,transform.position.z);
			for (int i = 0; i < frameRaycastPos.Length; ++i)
				frameRaycastPos [i] = new Vector2 (frameRaycastPos [i].x, frameRaycastPos [i].y + diff);
			flag |= (int)Lock.Down;
			grounded = true;
			jumpStatus = 0;
		}

		if ((frameRaycastPos [2].y >= frameMaxPos [3].y - collisionMargin)  || (frameRaycastPos [3].y >= frameMaxPos [3].y - collisionMargin)) {
			float diff = Mathf.Max (thisFramePos [2].y, thisFramePos [3].y) + collisionMargin - frameMaxPos[3].y;
			transform.position = new Vector3 (transform.position.x, transform.position.y - diff,transform.position.z);
			for (int i = 0; i < frameRaycastPos.Length; ++i)
				frameRaycastPos [i] = new Vector2 (frameRaycastPos [i].x, frameRaycastPos [i].y - diff);
			flag |= (int)Lock.Up;
			speed = new Vector2 (speed.x, 0);
		}

		return flag;
	}

	public void rotateTowardsMedian()
	{
		Quaternion.RotateTowards (transform.rotation, Quaternion.identity,Time.deltaTime * angleReductionCoefficient);
	}

	public void rotateToFit()
	{
		var DL = belows [0];
		var DR = belows [1];
		if (DL.y < DR.y) {
			Vector3 direction = new Vector3 (DL.x - DR.x, DL.y - DR.y, 0);
			Vector3 referenceDirection = new Vector3 (-1, 0, 0);
			Quaternion rotation = Quaternion.FromToRotation (referenceDirection, direction);
			transform.rotation = rotation;
		} else {
			Vector3 direction = new Vector3 (DR.x - DL.x, DR.y - DL.y, 0);
			Vector3 referenceDirection = new Vector3 (1, 0, 0);
			Quaternion rotation = Quaternion.FromToRotation (referenceDirection, direction);
			transform.rotation = rotation;
		}
	}
	//23
	//01
	Vector2[] getRayCastPositions()
	{
		Vector2 boxCollider = GetComponent<BoxCollider2D> ().size;
		Vector2 position = new Vector2 (GetComponent<BoxCollider2D>().bounds.center.x,GetComponent<BoxCollider2D>().bounds.center.y);
		Vector2[] rayCastPositions = new Vector2[4];
		rayCastPositions [0] = GetComponent<BoxCollider2D>().transform.TransformPoint(new Vector3 (- boxCollider.x / 2 - collisionMargin*2,- boxCollider.y / 2 - collisionMargin*2)).ToVector2();
		rayCastPositions [1] = GetComponent<BoxCollider2D>().transform.TransformPoint(new Vector3 (boxCollider.x / 2 + collisionMargin*2, - boxCollider.y / 2 - collisionMargin*2)).ToVector2();
		rayCastPositions [2] = GetComponent<BoxCollider2D>().transform.TransformPoint(new Vector3 (- boxCollider.x / 2 - collisionMargin*2,  boxCollider.y / 2 + collisionMargin*2)).ToVector2();
		rayCastPositions [3] = GetComponent<BoxCollider2D>().transform.TransformPoint(new Vector3 ( boxCollider.x / 2 + collisionMargin*2, boxCollider.y / 2 + collisionMargin*2)).ToVector2();
		return rayCastPositions;
	}
	//The Maxes are 0 - minX, 1 - maxX, 2 - minY 3 - maxY
	Vector2[] getMaxPositions(Vector2[] raycastPos)
	{
		LEnemies.Clear ();
		REnemies.Clear ();
		UEnemies.Clear ();
		DEnemies.Clear ();
		List<Vector2> XLRayHits = new List<Vector2> ();
		List<Vector2> XRRayHits = new List<Vector2> ();
		List<Vector2> YURayHits = new List<Vector2> ();
		List<Vector2> YDRayHits = new List<Vector2> ();

		Vector2 boxPosition = new Vector2 (GetComponent<BoxCollider2D>().bounds.center.x,GetComponent<BoxCollider2D>().bounds.center.y);

		foreach (Vector2 position in raycastPos) {
			int direction = 0;
			RaycastHit2D hit;
			if (position.x < boxPosition.x) {
				hit = Physics2D.Raycast (position, Left);
				direction = -1;
			} else {
				hit = Physics2D.Raycast (position, Right);
				direction = 1;
			}
			if (hit.collider != null && hit.collider.gameObject.layer != 12) {
				if (hit.collider.gameObject.layer == 11 && direction == 1)
					REnemies.Add (new EnemyHitInfo(hit.collider.gameObject,hit.point));
				else if (hit.collider.gameObject.layer == 11 && direction == -1)
					LEnemies.Add (new EnemyHitInfo(hit.collider.gameObject,hit.point));
				if (direction == 1)
					XRRayHits.Add (hit.point);
				else if (direction == -1)
					XLRayHits.Add (hit.point);
			}else if ((hit.collider == null || hit.collider.gameObject.layer == 12) && direction == 1) {
				XRRayHits.Add (new Vector2 (position.x + direction * 20, 0));
			} else {
				XLRayHits.Add (new Vector2 (position.x + direction * 20, 0));
			}
			if (position.y < boxPosition.y) {
				hit = Physics2D.Raycast (position, Down);
				direction = -1;
			} else {
				hit = Physics2D.Raycast (position, Up);
				direction = 1;
			}
			if (hit.collider != null && hit.collider.gameObject.layer != 12) {
				if (hit.collider.gameObject.layer == 11 && direction == 1)
					UEnemies.Add (new EnemyHitInfo(hit.collider.gameObject,hit.point));
				else if (hit.collider.gameObject.layer == 11 && direction == -1)
					DEnemies.Add (new EnemyHitInfo(hit.collider.gameObject,hit.point));
				if (direction == 1)
					YURayHits.Add (hit.point);
				else if (direction == -1)
					YDRayHits.Add (hit.point);
			}else if ((hit.collider == null || hit.collider.gameObject.layer == 12) && direction == 1) {
				YURayHits.Add (new Vector2 (0, position.y + direction * 20));
			} else {
				YDRayHits.Add (new Vector2 (0, position.y + direction * 20));
			}
		}

		var maxes = new Vector2[4];
		maxes [0] = findMaxX (XLRayHits);
		maxes [1] = findMinX (XRRayHits);
		maxes [2] = findMaxY (YDRayHits);
		maxes [3] = findMinY (YURayHits);
		belows [0] = YDRayHits [0];
		belows [1] = YDRayHits [1];
		return maxes;
	}

	public void checkForEnemyContact()
	{
		foreach (EnemyHitInfo info in LEnemies) {
			if (info.position.x > frameRaycastPos [0].x - collisionMargin * 2) {
				Hurt (Left);
			}
		}
		foreach (EnemyHitInfo info in REnemies) {
			if (info.position.x < frameRaycastPos [1].x + collisionMargin * 2) {
				Hurt (Right);
			}
		}
		foreach (EnemyHitInfo info in UEnemies) {
			if (info.position.y < frameRaycastPos [2].y + collisionMargin * 2) {
				Hurt (Up);
			}
		}
		foreach (EnemyHitInfo info in DEnemies) {
			if (info.position.y > frameRaycastPos [0].y - collisionMargin * 2) {
				Hurt (Down);
			}
		}
	}

	private void Hurt(Vector2 Direction)
	{
		if (Direction == Left || Direction == Right) {
			grounded = false;
			speed = new Vector2 (-Direction.x * maxSpeed, maxSpeed);
		} else if (Direction == Up) {
			speed = new Vector2 (0, -maxSpeed);
		} else if (Direction == Down) {
			int randomDirection = Random.Range (0, 1);
			if (randomDirection == 0)
				randomDirection = -1;
			speed = new Vector2 (randomDirection * maxSpeed, maxSpeed);
			grounded = false;
		}
		GetComponent<SpriteRenderer> ().color = new Color (1, 0, 0);
		StartCoroutine("resetColor");
	}

	private IEnumerator resetColor()
	{
		yield return new WaitForSeconds (0.1f);
		GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1);
	}

	public Vector2 findMinX(List<Vector2> points)
	{
		Vector2 min = points [0];
		foreach (var point in points)
			if (point.x < min.x)
				min = point;
		return min;
	}

	public Vector2 findMinY(List<Vector2> points)
	{
		Vector2 min = points [0];
		foreach (var point in points)
			if (point.y < min.y)
				min = point;
		return min;
	}

	public Vector2 findMaxX(List<Vector2> points)
	{
		Vector2 min = points [0];
		foreach (var point in points)
			if (point.x > min.x)
				min = point;
		return min;
	}

	public Vector2 findMaxY(List<Vector2> points)
	{
		Vector2 min = points [0];
		foreach (var point in points)
			if (point.y > min.y)
				min = point;
		return min;
	}
}

public class EnemyHitInfo
{
	public EnemyHitInfo(GameObject obj, Vector2 pos)
	{
		enemy = obj;
		position = pos;
	}
	public GameObject enemy;
	public Vector2 position;
}

	
public class XSorter : IComparer<Vector2>
{
	public int Compare(Vector2 first, Vector2 second)
	{
		if (second.x > first.x)
			return 1;
		else
			return -1;
	}
}

public class YSorter : IComparer<Vector2>
{
	public int Compare(Vector2 first, Vector2 second)
	{
		if (second.y > first.y)
			return 1;
		else
			return -1;
	}
}

public static class Vector3Extensions
{
	public static Vector2 ToVector2(this Vector3 vec)
	{
		return new Vector2 (vec.x, vec.y);
	}
}